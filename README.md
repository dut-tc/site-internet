![](images/engager-un-developpeur-wordpress-.png)


## ![logo](images/WordpressMiniLgo.png) [**Pourquoi choisir le CMS WordPress**](https://prezi.com/view/ZL2O0AuAM0ArQfn5yZgB/)

## ![logo](images/WordpressMiniLgo.png) [**Hébergement de son site WordPress**](https://prezi.com/view/FtXRTeAHQvMo0wywZVfO/)

## ![logo](images/WordpressMiniLgo.png) [**Connection au tableau de bord WordPress**](https://prezi.com/view/pwoJ68opEewwHQQ5NGFQ/)

# **[RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) : Règlement Général sur la Protection des Données** 

![gestiondeprojet](https://gitlab.com/dut-tc/internet/raw/master/images/gestiondeprojet.png)  **Rédiger un article sur le RGPD.**

# **Quelles sont les mentions obligatoires sur un site internet ?**

## **Mentions propres au site du particulier**

Le particulier qui crée un site internet n'est pas obligé de révéler son identité. Il doit cependant communiquer à l'hébergeur de son site les éléments qui permettront de l'identifier en cas de besoin. Ces éléments d'identification sont couverts par le secret professionnel. Mais l'hébergeur du site internet a l'obligation de les dévoiler dans le cadre d'une procédure judiciaire.

Si le particulier choisit de garder l'anonymat, les seules mentions obligatoires à retrouver sur son site internet sont : nom, dénomination ou raison sociale et adresse et numéro de téléphone de l'hébergeur de son site internet.

Si le particulier ne souhaite pas garder l'anonymat, il doit indiquer les mentions légales le concernant lui-même et celles de l'hébergeur de son site internet.

> Attention : le non-respect de ces obligations peut être sanctionné d'une peine de prison pouvant aller jusqu'à 1 an et de 75 000 € d'amende.

## **Mentions relatives à l'utilisation de cookies**

Un cookie est un petit fichier informatique qui permet d'analyser le comportement des usagers lors de la visite d'un site internet ou de l'utilisation d'un logiciel ou d'une application mobile.

Les éditeurs de sites ou d'applications qui utilisent des cookies doivent :

+ informer les internautes de la finalité des cookies,
+ obtenir leur consentement,
+ fournir aux internautes un moyen de les refuser.
+ La durée de validité du consentement donné dans ce cadre est de 13 mois maximum.

## **Mentions relatives à l'utilisation de données personnelles**

Les sites qui utilisent des données personnelles doivent obligatoires mentionner les informations suivantes :

+ Coordonnées du délégué à la protection des données (DPO ou DPD) de l'organisme, s'il a été désigné, ou d'un point de contact sur les questions de protection des données personnelles
+ Finalité poursuivie par le traitement auquel les données sont destinées
+ Caractère obligatoire ou facultatif des réponses et conséquences éventuelles à l'égard de l'internaute d'un défaut de réponse
+ Destinataires ou catégories de destinataires des données
+ Droits d'opposition, d'interrogation, d'accès et de rectification
+ Au besoin, transferts de données à caractère personnel envisagés à destination d'un État n'appartenant pas à l'Union européenne
+ Base juridique du traitement de données (c'est-à-dire ce qui autorise légalement le traitement : il peut s'agir du consentement des personnes concernées, du respect d'une obligation prévue par un texte, de l'exécution d'un contrat notamment)
+ Droit d'introduire une réclamation auprès de la Cnil

> Attention : l'absence d'une information obligatoire est punie d'une amende de 1 500 €. Tout traitement informatique non consenti des données recueillies est puni de 5 ans d'emprisonnement et de 300 000 € d'amende.

# **[Générateur de mentions légales](https://www.subdelirium.com/generateur-de-mentions-legales/)**


![serviceP](images/logo-service-public-pro.png)

# **[Site Professionnel](https://www.service-public.fr/professionnels-entreprises/vosdroits/F31228)**
